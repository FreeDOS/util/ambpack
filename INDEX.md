# AMB Packager

archiver that packs/unpacks AMB (Ancient Machine Book) files


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## AMBPACK.LSM

<table>
<tr><td>title</td><td>AMB Packager</td></tr>
<tr><td>version</td><td>20201217</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-02-22</td></tr>
<tr><td>description</td><td>archiver that packs/unpacks AMB (Ancient Machine Book) files</td></tr>
<tr><td>keywords</td><td>text, doc, archiver</td></tr>
<tr><td>author</td><td>Mateusz Viste</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mateusz Viste</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://sourceforge.net/projects/ambook/</td></tr>
<tr><td>original&nbsp;site</td><td>http://amb.osdn.io</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>MIT</td></tr>
</table>
