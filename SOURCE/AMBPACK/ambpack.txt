
AMBPACK is an archiver that packs/unpacks AMB (Ancient Machine Book) files.

homepage: http://amb.osdn.io

Run ambpack without arguments for usage help.


=== HISTORY ==================================================================

ver 20201217:
 - added 'cc' and 'uc' actions (conversion from/to UTF-8)
 - checks for filename duplicates when creating an AMB archive
 - error when trying to create an AMB archive without an 'index.ama' article
 - error when trying to pack a filename that contains high-ascii characters
 - warning displayed when creating an AMB archive without a 'title' file
 - fixed unpacking of archives with more than 255 entries

ver 20201210:
 - fixed procedure that unpacks files

ver 20201204:
 - attempt to pack a file bigger than 65535 bytes triggers an error
 - Windows compatibility

ver 20201201:
 - first public release


=== LICENSE ==================================================================

This software is published under the terms of the MIT license.

Copyright (C) 2020 Mateusz Viste

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
